--[[
    IP-Functions

    (c) 2011 by Thomas
--]]
function isModuleAvailable(name)
   if package.loaded[name] then
      return true
   else
      for _, searcher in ipairs(package.searchers or package.loaders) do
         local loader = searcher(name)
         if type(loader) == 'function' then
            package.preload[name] = loader
            return true
         end
      end
      return false
   end
end -- isModuleAvailable()

local bit;
if (isModuleAvailable('bit32')) then
   bit = require('bit32');
else
   -- Policy: for luajit use 'bit'
   bit = require('bit');
end

-- required functions
local function strsplit(delimiter, text)
  local list = {}
  local pos = 1
  if string.find("", delimiter, 1) then -- this would result in endless loops
    error("delimiter matches empty string!")
  end
  while 1 do
    local first, last = string.find(text, delimiter, pos)
    if first then -- found?
      table.insert(list, string.sub(text, pos, first-1))
      pos = last+1
    else
      table.insert(list, string.sub(text, pos))
      break
    end
  end
  return list
end



function check_ip(ip)
    local i = 1
    local octets = 1
    local octet = ""
    local err = ""
    local errors = 0
    local blocks = { }
    while (i <= string.len(ip) + 1) do
        local c = string.sub(ip, i, i)
        if c == "." or string.len(ip)+1 == i then
            octets = octets + 1
            local block = tonumber(octet)
            if block == nil or block > 255 or block < 0 or octets > 5 then
                errors = errors + 1
                err = err .. " \f3>" .. (block or "?") .. "<\f8"
            else
                blocks[#blocks + 1] = octet
                err = err .. "\f0 " .. block .. "\f8"
            end
            octet = ""
        else
            octet = octet .. c
        end
        i = i + 1
    end

    if errors > 0 then
        return { err }
    end

    return { 0, blocks }
end

function ip2long(ip_addr)
    local blocks = check_ip(ip_addr)[2] or error("Invalid IP-Address")
    --return (blocks[1] << 24) | (blocks[2] << 16) | (blocks[3] << 8 ) | blocks[4];
    return bit.bor(bit.lshift(blocks[1], 24) , bit.lshift(blocks[2], 16) , bit.lshift(blocks[3], 8 ), blocks[4]);
end

local function us2signed(num)
    if(num < 0) then
       return 256 + num;
    else
       return num;
    end
end

function long2ip(addr)
    if not addr or type(addr) ~= "number" then error("Invalid Long-IP-Address") end
    local a = bit.rshift(bit.band(addr, (0xff000000)), 24);
    local b = bit.rshift(bit.band(addr, (0x00ff0000)), 16);
    local c = bit.rshift(bit.band(addr, (0x0000ff00)), 8);
    local d = bit.band(addr, 0xff);
    return string.format("%i.%i.%i.%i", us2signed(a), us2signed(b), us2signed(c), us2signed(d))
end

