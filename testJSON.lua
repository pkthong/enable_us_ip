require('strict');
require('network');

local JSON = (loadfile "JSON.lua")() --load the routines
JSON.strictTypes = true

function readAll(file)
   local f = io.open(file, "rb");
   local content = f:read("*all");
   f:close();
   return content;
end

function writeAll(file, fileContents)
   local f = io.open(file, "wb");
   local content = f:write(fileContents);
   f:close();
   return;
end

function generate_ifconfig(catchall_ipTable, proxiesTable, outputFileName)
   local fileText = "";
   local interfaceCount = 0;
   for catchall_ipKey, catchall_ipValue in ipairs(catchall_ipTable) do
      fileText = fileText ..
         string.format("ifconfig eth0:%d %s netmask 255.255.255.0\n ifconfig eth0:%d up\n",
            interfaceCount,
            catchall_ipValue,
            interfaceCount);
      interfaceCount = interfaceCount + 1;
   end
   for proxyKey, proxyValue in ipairs(proxiesTable) do
      if (proxyValue['ip'] ~= nil) then
         fileText = fileText ..
            string.format("ifconfig eth0:%d %s netmask 255.255.255.0\n ifconfig eth0:%d up\n",
               interfaceCount,
               proxyValue['ip'],
               interfaceCount);
         interfaceCount = interfaceCount + 1;
      end
   end

   writeAll(outputFileName, fileText);
   print(string.format("Generated %s", outputFileName));
   return;
end

function generate_haproxyHeader()
   return [[
global
  daemon
  maxconn 20000
  user haproxy
  group haproxy
  stats socket /var/run/haproxy.sock mode 0600 level admin
  log /dev/log local0 debug
  pidfile /var/run/haproxy.pid
  spread-checks 5

defaults
  maxconn 19500
  log global
  mode http
  option httplog
  option abortonclose
  option http-server-close
  option persist
  timeout connect 20s
  timeout client 120s
  timeout server 120s
  timeout queue 120s
  timeout check 10s
  retries 3

]];
end

function generate_haproxy_stats (statsTable)
   local retText = "";
   if (statsTable['enabled']) then
      retText = string.format( [[
listen stats
  bind *:%d
  mode http
  stats enable
  stats realm Protected\ Area
  stats uri /
  stats auth %s:%s

]],
   statsTable['port'],
   statsTable['user'],
   statsTable['password']);
   end
   return retText;
end -- generate_haproxy_stats()

function generate_haproxy_catchall_http(proxiesTable, catchall_http_IP, catchall_http_port)
   local retText = "";
   retText = string.format([[
frontend f_catchall_http
  bind *:%d
  log global
  mode http
  option httplog
  capture request header Host len 50
  capture request header User-Agent len 150
  default_backend b_deadend_http

]],
   catchall_http_port
   );

   for proxiesKey, proxiesValue in ipairs(proxiesTable) do
      if (proxiesValue['enabled'] and proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^http$") ~= nil) then
               retText = retText .. string.format(
                  "  use_backend b_catchall_http if { hdr(host) -i %s }\n",
                  proxiesValue['dest_addr']
                  );
            end
         end
      end
   end

   retText = retText .. "\n";
   retText = retText ..
   [[
backend b_catchall_http
  log global
  mode http
  option httplog
  option accept-invalid-http-response
]];

   for proxiesKey, proxiesValue in ipairs(proxiesTable) do
      if (proxiesValue['enabled'] and proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^http$") ~= nil) then
               retText = retText .. string.format(
[[
  use-server %s if { hdr(host) -i %s }
  server %s %s:80 check inter 10s fastinter 2s downinter 2s fall 1800

]],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr']
               );
            end
         end
      end
   end

   retText = retText .. "\n";

   return retText;
end -- generate_haproxy_catchall_http()

function generate_haproxy_catchall_https(proxiesTable, catchall_https_IP, catchall_https_port)
   local retText = "";

   -- generate https frontend
   retText = string.format([[
frontend f_catchall_https
  bind *:%d
  log global
  mode tcp
  option tcplog
  tcp-request inspect-delay 5s
  tcp-request content accept if { req_ssl_hello_type 1 }
  default_backend b_deadend_https
]],
   catchall_https_port
   );

   retText = retText .. "\n";

   for proxiesKey, proxiesValue in ipairs(proxiesTable) do
      if (proxiesValue['enabled'] and proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^https$") ~= nil) then
               retText = retText .. string.format(
               [[
  use_backend b_catchall_https if { req_ssl_sni -i %s }

]],
               proxiesValue['dest_addr']
               );
            end
         end
      end
   end
   retText = retText .. "\n";

   -- generate https backend
   retText = retText .. string.format([[
backend b_catchall_https
  log global
  mode tcp
  option tcplog
]]);

   for proxiesKey, proxiesValue in ipairs(proxiesTable) do
      if (proxiesValue['enabled'] and proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^https$") ~= nil) then
               retText = retText .. string.format(
               [[
  use-server %s if { req_ssl_sni -i %s }
  server %s %s:443 check inter 10s fastinter 2s downinter 2s fall 1800

]],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr'],
               proxiesValue['dest_addr']
               );
            end
         end
      end
   end
   retText = retText .. "\n";
   return retText;
end -- generate_haproxy_catchall_https()

function generate_haproxy_non_sni(proxiesTable)
   local retText = "";

   for proxiesKey, proxiesValue in ipairs(proxiesTable) do
      if (proxiesValue['enabled'] and not proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^http$") ~= nil) then
               retText = retText .. string.format(
[[
frontend f_%s_http
  bind *:%d
  log global
  mode http
  option httplog
  capture request header Host len 50
  capture request header User-Agent len 150
  default_backend b_%s_http

backend b_%s_http
  log global
  mode http
  option httplog
  option accept-invalid-http-response
  server %s %s:80 check inter 10s fastinter 2s downinter 2s fall 1800

]],
                  proxiesValue['name'],
                  modesValue['port'],
                  proxiesValue['name'],
                  proxiesValue['name'],
                  proxiesValue['dest_addr'],
                  proxiesValue['dest_addr']
                  );
            else if (string.match(modesValue['mode'], "^https$") ~= nil) then
               retText = retText .. string.format(
[[
frontend f_%s_https
  bind *:%d
  log global
  mode tcp
  option tcplog
  default_backend b_%s_https

backend b_%s_https
  log global
  mode tcp
  option tcplog
  server %s %s:443 check inter 10s fastinter 2s downinter 2s fall 1800

]],
                  proxiesValue['name'],
                  modesValue['port'],
                  proxiesValue['name'],
                  proxiesValue['name'],
                  proxiesValue['dest_addr'],
                  proxiesValue['dest_addr']
                  );
               end
            end
         end
      end
   end
   retText = retText ..
[[
backend b_deadend_http
  log global
  mode http
  option httplog
  option accept-invalid-http-response
  option http-server-close

backend b_deadend_https
  log global
  mode tcp
  option tcplog
]]

   return retText;
end -- generate_haproxy_non_sni()

function generate_ipTables(configTable)
   local retText = "";
   for proxiesKey, proxiesValue in ipairs(configTable['proxies']) do
      if (proxiesValue['enabled'] and not proxiesValue['catchall']) then
         for modesKey, modesValue in ipairs(proxiesValue['modes']) do
            if (string.match(modesValue['mode'], "^http$") ~= nil) then
               retText = retText .. string.format(
[[
iptables -t nat -A PREROUTING -p tcp --dport 80 -d %s -j DNAT --to-destination %s:%d
iptables -t nat -A POSTROUTING -p tcp --dport %d -j MASQUERADE
]],
                  proxiesValue['ip'],
                  configTable['haproxy_bind_ip'],
                  modesValue['port'],
                  modesValue['port']
                  );

            else if (string.match(modesValue['mode'], "^https$") ~= nil) then
               retText = retText .. string.format(
[[
iptables -t nat -A PREROUTING -p tcp --dport 443 -d %s -j DNAT --to-destination %s:%d
iptables -t nat -A POSTROUTING -p tcp --dport %d -j MASQUERADE
]],
                  proxiesValue['ip'],
                  configTable['haproxy_bind_ip'],
                  modesValue['port'],
                  modesValue['port']
                  );
               end
            end
         end
      end
   end

   return retText;
end -- generate_ipTables()

function generate_dnsmasq(configTable)
   local retText = "";
   for proxiesKey, proxiesValue in ipairs(configTable['proxies']) do
      if (proxiesValue['enabled']) then
         if (proxiesValue['catchall']) then 
               retText = retText .. string.format(
[[
address=/%s/%s
]],
               proxiesValue['dest_addr'],
               configTable['haproxy_bind_ip']
               );

         else
               retText = retText .. string.format(
[[
address=/%s/%s
]],
               proxiesValue['dest_addr'],
               proxiesValue['ip']
               );
         end
      end
   end

   return retText;
end -- generate_dnsmasq()

-- main()
local fileText = readAll("config.json");
--print (fileText);
local configJSON = JSON:decode(fileText);

local inspect = require ('inspect');
--print(inspect(jsonDecodeTable));

local ipTablesFileName = "ipTables-haproxy.cfg"
local IP_count = configJSON["dnat_base_ip"];
local currentPort = configJSON["dnat_base_port"];
local base_IP_count = IP_count;

-- Assign bind ip
local handle = io.popen("dig +short testmachinenew.cloudapp.net | tr -d '\n' ");
configJSON['haproxy_bind_ip'] = handle:read('*a');
handle.close();

-- Assign IP and port to catchall http
local catchall_http_IP = IP_count;
local catchall_http_port = 80;
IP_count = long2ip(ip2long(IP_count)+1);

-- Assign IP and port to catchall https
local catchall_https_IP = IP_count;
local catchall_https_port = 443;
IP_count = long2ip(ip2long(IP_count)+1);

-- Assign IP and port to the 'enabled' proxies
for proxyKey,proxyValue in ipairs(configJSON["proxies"]) do
   if (proxyValue['enabled'] and not proxyValue['catchall']) then
      proxyValue['ip'] = IP_count;
      for modesKey, modesValue in ipairs(proxyValue['modes']) do
	 modesValue['port'] = currentPort;
         currentPort = currentPort + 1;
      end
      IP_count = long2ip(ip2long(IP_count)+1);
   end
end

-- generate script for ifconfig
generate_ifconfig({catchall_http_IP, catchall_https_IP}, configJSON['proxies'], "ifconfig_enableUS_IP.sh");

-- generate script for haproxy
local haproxyText = generate_haproxyHeader();
haproxyText = haproxyText .. generate_haproxy_stats(configJSON['stats']);
haproxyText = haproxyText .. generate_haproxy_catchall_http(
   configJSON['proxies'],
   catchall_http_IP,
   catchall_http_port
   );
haproxyText = haproxyText .. generate_haproxy_catchall_https(
   configJSON['proxies'],
   catchall_https_IP,
   catchall_https_port
   );
haproxyText = haproxyText .. generate_haproxy_non_sni(
   configJSON['proxies']
   );
writeAll("haproxy_enableUS_IP.cfg", haproxyText);

-- generate script for iptables
local ipTablesText = generate_ipTables(configJSON);
writeAll("ipTables_enableUS_IP.sh", ipTablesText);

-- generate dnsmasq
local dnsmasqText = generate_dnsmasq(configJSON);
writeAll("dnsmasq_enableUS_IP.conf", dnsmasqText);
